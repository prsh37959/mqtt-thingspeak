'''
Inspiration from http://www.steves-internet-guide.com/into-mqtt-python-client/
created by: Nikolaj Simonsen | November 2021
repo: https://gitlab.com/npes-py-experiments/mqtt-thingspeak

paho-mqtt lib reference: https://pypi.org/project/paho-mqtt/
'''

# IMPORTS

from functions import get_tokens
from socket import error as socket_error
import paho.mqtt.client as mqtt
from datetime import datetime
import json

# VARIABLES

# get secret device credentials from thingspeak MQTT textfile 
try: 
    tokens = get_tokens(filename='thingspeak_device2_credentials.txt')
except Exception as error:
    print(f'Error reading credentials: {error}')

username = tokens.get('username')
clientId = tokens.get('clientId')
password = tokens.get('password')
broker_addr = 'mqtt3.thingspeak.com'  
broker_port = 1883 # for tcp if port 1883 is open
# broker_port = 80 # for websocket if port 1883 is blocked
channel_id = 1150771 # replace with your thingspeak channel_id
subscribe_topic = f'channels/{channel_id}/subscribe'
publish_interval = 15
client = mqtt.Client(client_id=clientId, clean_session=1, transport='tcp') # init client object
#client = mqtt.Client(client_id=clientId, clean_session=1, transport='websockets') # init client object using websockets if port 1883 is blocked
client.username_pw_set(username=username, password=password) # set client credentials

# FUNCTIONS

# triggers when message received from broker
def on_message(client, userdata, message):
    message_decoded = json.loads(message.payload.decode('utf-8'))
    print(f'- message received, {message_decoded}\n')

# triggers when connected to mqtt broker
def on_connect(client, userdata, flags, rc):
    print(f'- client: {client} connected to {broker_addr} on port {broker_port}, userdata: {userdata}, flags: {flags}, rc: {rc}\n')

# attach callbacks
client.on_connect = on_connect # attach callback function
client.on_message = on_message # attach message callback to callback

# MAIN PROGRAM

try: 
    print('- connecting to broker')
    client.connect(host=broker_addr, port=broker_port)
    client.subscribe(subscribe_topic, qos=0)
    client.loop_start() #start the event loop

except socket_error as e:
    print(f'- could not connect {clientId} to {broker_addr} on port {broker_port}\n {e}')
    exit(0)

# publish
while(True):
    try:
        # nothing here as subsrcibed message is handled in the on_message callback
        pass

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        exit(0)